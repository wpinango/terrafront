import Vue from 'vue'
import Vuex from 'vuex'
//import axios from 'axios'
import router from './router';

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    accessToken: null,
    loggingIn: false,
    loginError: null,
    userData: null,
    drawer: null,
    color: 'primary',
    image: 'http://www.mobileswall.com/wp-content/uploads/2013/09/901-3D-Lacza-l.jpg'
  },
  mutations: {
    loginStart: state => state.loggingIn = true,
    loginStop: (state, errorMessage) => {
      state.loggingIn = false;
      state.loginError = errorMessage;
    },
    updateAccessToken: (state, accessToken) => {
      state.accessToken = accessToken;
    },
    updateUserData:(state, userData) => {
      state.userData = userData;
    },
    logout: (state) => {
      state.accessToken = null;
    },
    setDrawer: (state, value) => {
      state.drawer = value;
    }
  },
  actions: {
    successLogin({ commit },token, userData) {
      localStorage.setItem('accessToken', token);
      localStorage.setItem('userData', userData);
      commit('loginStop', null);
      commit('updateAccessToken', token);
      commit('updateUserData', userData);
      router.push('/dashboard');
    },
    errorLogin({ commit },error) {
      commit('loginStop', error.response.data);
      commit('updateAccessToken', null);
      commit('updateUserData', null);
    },
    fetchAccessToken({ commit }) {
      commit('updateAccessToken', localStorage.getItem('accessToken'));
    },
    logout({ commit }) {
      localStorage.removeItem('accessToken');
      localStorage.removeItem('userData');
      commit('logout');
      router.push('/login');
    }
  }
})