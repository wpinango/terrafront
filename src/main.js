import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSweetalert2 from 'vue-sweetalert2';
import router from './router';
import store from './store';
import { sync } from 'vuex-router-sync';
import Vuex from 'vuex';

import 'sweetalert2/dist/sweetalert2.min.css';

import vuetify from './plugins/vuetify';


Vue.use(VueMaterial)
Vue.use(VueAxios, axios)
Vue.use(VueSweetalert2);
Vue.use(Vuex);

sync(store, router);


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
