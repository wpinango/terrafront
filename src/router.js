import Vue from 'vue'
import Router from 'vue-router'
import store from './store';
import Login from '@/components/login/Login.vue'
import Dashboard from '@/components/dashboard/Dashboard.vue'
import DashboardGroup from '@/components/dashboard/DashboardGroup.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/dashboardGroup',
      name: 'dashboardGroup',
      component: DashboardGroup
    },
    {
      path: '*',
      redirect: '/login'
    }
  ]
})

router.beforeEach((to, from, next) => {
  store.dispatch('fetchAccessToken');
  if (to.fullPath === '/dashboard') {
    if (!store.state.accessToken) {
      next('/login');
    }
  }
  if (to.fullPath === '/login') {
    if (store.state.accessToken) {
      next('/dashboard');
    }
  }
  next();
});

export default router;